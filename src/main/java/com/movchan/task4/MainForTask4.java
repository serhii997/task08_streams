package com.movchan.task4;

import org.xlsx4j.sml.Col;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainForTask4 {
    private List<Person> people;

    public MainForTask4() {
        people = Arrays.asList(new Person("Sergio", 22),
                new Person("Nastia",21),
                new Person("Vlad",22),
                new Person("Rostik",23),
                new Person("Nastia",21));
    }

    public void run(){
        people.stream().forEach(System.out::println);
        List<Integer> uniqPerson = people.stream().map(Person::getAge)
                .distinct()
                .collect(Collectors.toList());
        System.out.println("count = " +uniqPerson.size());

        for (Integer old : uniqPerson) {
            System.out.print(old + " n=");
            long count = people.stream().filter(person -> person.getAge() == old)
                    .count();
            System.out.println(count);
        }

        List<Integer> sort = uniqPerson.stream().distinct()
                .sorted()
                .collect(Collectors.toList());
        System.out.println();
        sort.stream().forEach(System.out::println);

    }

}

package com.movchan.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MaxValue {
    private final static Logger LOGGER = LogManager.getLogger(MaxValue.class);
    public void run(){
        MyLambda myLambda = ((a, b, c) -> {
            if( a > b && a > c){
                return a;
            }else if( b > a && b > c){
                return b;
            }else if( c > a && c > b){
                return c;
            }else return 0;
        });
        int fun = myLambda.fun(5, 8, 3);
        LOGGER.info(fun);

        MyLambda myLambda1 = ((a, b, c) -> (a+b+c)/3);

        int fun1 = myLambda1.fun(5, 5, 5);
        LOGGER.info(fun1);
        //System.out.println("average = " + myLambda1.fun(5,5,5));
    }

}
@FunctionalInterface
interface MyLambda{
    int fun(int a, int b, int c);
}


package com.movchan;

import com.movchan.task1.MaxValue;
import com.movchan.task2.MainForSecondTask;
import com.movchan.task3.MainForTask3;
import com.movchan.task4.MainForTask4;

public class Application {
    public static void main(String[] args) {
        new MaxValue().run();
        new MainForTask3().run();
        new MainForTask4().run();
        new MainForSecondTask();
    }
}

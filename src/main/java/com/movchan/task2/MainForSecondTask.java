package com.movchan.task2;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainForSecondTask {
    private Map<Execute, Printable> menu;
    private static Scanner sc = new Scanner(System.in);

    public MainForSecondTask() {
        menu = new LinkedHashMap<>();
        menu.put(Execute.LAMBDA, this::runLambda);
        menu.put(Execute.ANONYMOUS, this::runAnonymous);
        menu.put(Execute.REFERENCE, this::runReference);

    }

    private void runLambda(){
        myExecute myExecute = value -> System.out.println(value);
    }
    private void runAnonymous(){
        System.out.println(new myExecute() {
            @Override
            public void execute(String value) {
                System.out.println(value);
            }
        });
    }
    private void runReference(){
        myExecute myExecute = System.out::println;
    }

}

@FunctionalInterface
interface myExecute{
    void execute(String value);
}

package com.movchan.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MainForTask3 {
    private final static Logger LOGGER = LogManager.getLogger(MainForTask3.class);
    private List<Integer> list;

    public MainForTask3() {
        list = new ArrayList<>();
        fillList();
    }

    public void run(){
        list.stream().forEach(System.out::println);
        System.out.println(list.stream().count());
        System.out.println(list.stream().min(Integer::compareTo));
        System.out.println(list.stream().max(Integer::compareTo));
        Optional<Integer> reduce = list.stream().reduce((i, j) -> i + j);
        System.out.println(reduce);
        OptionalDouble average = list.stream().mapToInt(value -> value).average();
        System.out.println(average);
        Long aLong = list.stream()
                .filter(integer -> integer > average.getAsDouble())
                .count();
        System.out.println(aLong);
    }

    private void fillList(){
        for (int i = 0; i < 10; i++) {
            list.add(new Random().nextInt(100));
        }
    }

}
